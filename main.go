package main

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"time"
)

type (
	Monitor struct {
		Name string `json:"name`
	}
)

func getWallpapers(r string) []string {
	var files []string = []string{}

	filepath.Walk(r, func(path string, info os.FileInfo, err error) error {
		switch filepath.Ext(path) {
		case ".jpg", ".jpeg", ".png":
			files = append(files, path)
		}
		return nil
	})
	return files
}

func getMonitor() []string {
	mon := make([]string, 0)
	monitors := make([]Monitor, 0)
	out, _ := exec.Command("swaymsg", "-t", "get_outputs").Output()
	err := json.Unmarshal([]byte(out), &monitors)
	if err != nil {
		os.Exit(1)
	}
	for _, v := range monitors {
		mon = append(mon, v.Name)
	}
	return mon
}

func setWallPaper(m []string, p []string, met string) {
	for _, v := range m {
		ix := rand.Intn(len(p))
		exec.Command("swaymsg", "output", v, "bg", p[ix], met, "#000000").Run()
	}
	fmt.Printf("\n\n")
}

func usage() {
	fmt.Printf("Usage:\n    mooswaybg 60 /path/to/Wallpapers fill\n\nThis mean to change wallpapers every 60 seconds from /path/to/Wallpapers with fill method (stretch fill fit center tile)\n\n")
}

func main() {
	rand.Seed(time.Now().UnixNano())
	if len(os.Args) != 4 {
		usage()
		return
	}
	second, err := strconv.ParseInt(os.Args[1], 10, 32)
	if err != nil {
		usage()
		return
	}
	fn := getWallpapers(os.Args[2])
	setWallPaper(getMonitor(), fn, os.Args[3])
	tic := time.Tick(time.Duration(second) * time.Second)
	for _ = range tic {
		setWallPaper(getMonitor(), fn, os.Args[3])
	}
}
